import azure.functions as func
import logging
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient

def orchestrator_function(context: df.DurableOrchestrationContext):
    connection_str = "DefaultEndpointsProtocol=https;AccountName=mapreduceanis2;AccountKey=wzMNqqJvvyWOMbYlwdBzyIqlhdI420zwdHW8kZVmTtFqB0h7nQeaTcvfx4oEJaaKnoOdVTdqteav+AStwVY2sw==;EndpointSuffix=core.windows.net"
    container_name = "anis"
    blob_service_client = BlobServiceClient.from_connection_string(connection_str)
    container_client = blob_service_client.get_container_client(container_name)

    # List the blobs in the container
    blobs = container_client.list_blobs()

    # List to store lines from all files
    lines_all = []

    # Fetch and store lines from each blob in the container
    for blob in blobs:
        blob_client = container_client.get_blob_client(blob)
        content = blob_client.download_blob().readall().decode('utf-8')
        lines = content.split('\n')  # Split content into lines
        lines_all.extend(lines)
    # Mapping phase
    tasks0 = []
    for line_string in lines_all:
        tasks0.append(context.call_activity("Mapper", line_string))
    mapped_outputs = yield context.task_all(tasks0)

    # # Shuffling phase
    shuffled_data = yield context.call_activity("Shuffler", mapped_outputs)

    # Reducing phase
    tasks = []
    for a in list(shuffled_data.items()):
        tasks.append(context.call_activity("Reducer", a))
    reduced_results = yield context.task_all(tasks)

    return reduced_results

main = df.Orchestrator.create(orchestrator_function)