import logging

def main(mapOutputs):
    shuffled_data = {}
    
    for output_list in mapOutputs:
        for out in output_list:
            word = out[0]
            count = out[1]
            if word not in shuffled_data:
                shuffled_data[word] = [count]
            else:
                shuffled_data[word].append(count)
    
    return shuffled_data