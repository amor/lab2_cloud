import logging
import re

def main(lineAndNumber) -> list:
    line_string = lineAndNumber
    words = re.findall(r'\w+', line_string.lower())
    mapped_output = [(word, 1) for word in words]
    return mapped_output