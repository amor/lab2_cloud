def main(schOutput):
    key = schOutput[0]
    values = schOutput[1]
    total_count = sum(values)
    result = [key, total_count]
    return result