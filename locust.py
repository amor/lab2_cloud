from locust import HttpUser, task, between

class User(HttpUser):
    host = "https://functionappanis.azurewebsites.net/" 
    @task
    def test_integration_service(self):
        self.client.get("api/numericalintegralservice?min=0.0&max=3.14159")
