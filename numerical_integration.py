import math

def abs_sin(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0
    
    for i in range(N):
        xip12 =  lower + dx*(i+0.5)
        dI = dx* abs_sin(xip12)
        integral += dI 
        
    return  integral

lower = 0
upper = math.pi  

N_values = [10, 100, 1000, 10000, 100000, 1000000]

results = []
for N in N_values:
    result = numerical_integration(lower, upper, N)
    results.append(result)

for i, result in enumerate(results):
    print(f"N = {N_values[i]} : Result = {result}")
