import azure.functions as func
import logging
import json
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)
def abs_sin(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0
    
    for i in range(N):
        xip12 =  lower + dx*(i+0.5)
        dI = dx* abs_sin(xip12)
        integral += dI 
        
    return  integral

@app.route(route='numericalintegralservice')
def http_trigger(req: func.HttpRequest) -> func.HttpResponse:
    logging.info("we are in !!")
    min = req.params.get('min')
    max = req.params.get('max')
    if (not min) and (not max):
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            min = req_body.get('min')
            max = req_body.get('max') 
    try:
        logging.info("yes!!")
        lower = float(req.params.get('min'))
        upper = float(req.params.get('max'))
        if lower >= upper :
            return func.HttpResponse(f"please check the integral boundaries :)",status_code=200) 
        N_values = [10, 100, 1000, 10000, 100000, 1000000]
        results = []

        for N in N_values:
            result = numerical_integration(lower, upper, N)
            results.append(result)

        logging.info("almost there")    
        return func.HttpResponse(json.dumps(results),mimetype="application/json",status_code=200)
    except ValueError:
        return func.HttpResponse(f"your parameters should be float :), please change them.",status_code=200)