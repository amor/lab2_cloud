import math
from flask import Flask
from flask import request
app = Flask(__name__)
def abs_sin(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    dx = (upper - lower) / N
    integral = 0.0
    
    for i in range(N):
        xip12 =  lower + dx*(i+0.5)
        dI = dx* abs_sin(xip12)
        integral += dI 
        
    return  integral

@app.route('/numericalintegralservice/<float:lower>/<float:upper>')
def calculate_integral(lower, upper):
    N_values = [10, 100, 1000, 10000, 100000, 1000000]
    results = []

    for N in N_values:
        result = numerical_integration(lower, upper, N)
        results.append(result)

    return {
        "Lower Bound": lower,
        "Upper Bound": upper,
        "Results": dict(zip(N_values, results))
    }

if __name__ == '__main__':
    app.run(debug=True)
